# microsoft dynamics 365 user create

I created this script to eliminate the manual labor of creating multiple users on CRM built-with Microsoft Dynamics 365.

Script is pretty simple and straight forward.

- Create a plain-text file containing list of users to be created.
- Modify the config variables on line numbers 2,5,6 and 7.
- Execute the script.
