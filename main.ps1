$workdir = "$home\desktop"
$userlist = Get-Content -Path "$workdir\userlist.txt"
$logfile = "$workdir\output_$(Get-Date -Format "MMddyyyy_HHMMss_ffff").log"

$domain = "domain"
$envname = "hostname"
$subdomain = "subdomain"

$credential = Get-Credential -Credential ("$domain\$envname" + "usrsuffix")
$webrequest = Invoke-WebRequest -Uri "http://$subdomain.$envname.$domain/Administration/UserCreate/Create" -Credential $credential

$userlist | %{
	$userid = $_.ToLower()
	$webrequest.Forms.Fields.userName = "$domain\$envname$userid"
	$webrequest = Invoke-WebRequest -Uri "http://$subdomain.$envname.$domain/Administration/UserCreate/Create" -Credential $credential -Body $webrequest.Forms.Fields
	(($webrequest.Content.Split('<').Split('/') | Select-String -SimpleMatch -Pattern "textarea").Line[0].Split('>') | Select-Object -Last 1).Trim().Replace(' with &#39;','. ').Replace('&#39;','').Replace('created',"created with $userid") | Out-File -FilePath "$logfile" -Append
}
